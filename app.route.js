routing.$inject = ['$routeProvider', '$locationProvider'];

export default function routing($routeProvider, $locationProvider) {
  $routeProvider
    .when('/Demo', {
      templateUrl: 'app/demo/demo.html',
      controller: 'DemoController',
      controllerAs: 'vm'
    })
    .otherwise({
      redirectTo: '/Demo'
    });
}
