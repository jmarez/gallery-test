import angular from 'angular';

import AppCtrl from './AppCtrl.js';

import '../style/app.css';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'vm'
  }
};


const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [])
  .directive('app', app)
  .controller('AppCtrl', ['$scope', AppCtrl]);

export default MODULE_NAME;
