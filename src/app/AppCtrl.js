export default class AppCtrl {
  constructor($scope) {
    var vm = this;

    vm.dataUrl = 'http://agence-bmobile.com/RH/dev/4e4e5f2c7h6g/datas.json';
    vm.datas = {
    	"folderName": "boston",
    	"galleryName": "Boston and Cambridge (2002-2006)",
    	"images": [{
    		"description": "",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2914.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Sailboats on the Charles River",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "with Cambridge and the MIT Green Building (designed by I.M. Pei) in the background",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2938.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "People lying on the esplanade",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "A beautiful day along the Charles River Esplanade.  The MIT Great Dome can be seen in the background, along with a pretty construction crane.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/river.jpg",
    		"height": 600,
    		"month": 9,
    		"title": "Charles River and MIT Great Dome",
    		"width": 450,
    		"year": 2002
    	}, {
    		"description": "This picture was taken from the Cambridge side of the Charles River.  You can see the city of Boston in the background.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/boats.jpg",
    		"height": 525,
    		"month": 4,
    		"title": "Naked Sailboats in the Sunset",
    		"width": 700,
    		"year": 2003
    	}, {
    		"description": "MIT campus and sailboats in the background.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2941.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Man sitting on bench - Charles River Esplanade",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Look at how green it is in the springtime!",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2939.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Benches on the Charles River Esplanade",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Beyond the Charles River, there is Cambridge on the left, Boston on the right, and the Longfellow Bridge connecting the two in the center.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2962.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Biker on Esplanade Platform",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "John Hancock Building on the left and Prudential Tower on the right",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2965.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Esplanade with Boston in the Background",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "The Boston Marathon is a great excuse for people to get drunk during the daytime ('cause nobody has to go to work on Patriots' Day weekend), so the Captain does his part to help.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2761.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Captain Morgan at the Boston Marathon",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "Lots of these folks are probably drunk ...",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2764.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "People on Boylston Watching Boston Marathon",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Check out the Matrix-looking guy next to the TV camera stand.  Of course, my favorite building, the John Hancock Tower in the background, as well as flags of many countries lining the street near the finish line.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2772.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Near the Boston Marathon Finish Line",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "About 1 mile left until the finish.  People running near the Citgo sign in Boston.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2781.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "1 Mile from Boston Marathon Finish Line",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "About 1 mile left until the finish.  People running near the Citgo sign in Boston.  Some are so pooped by this time that they are walking.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2783.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Boston Marathon Runners",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "About 1 mile left until the finish.  John Hancock and Prudential towers in the distant background.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2785.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Boston Marathon Runners (again)",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "I snapped this candid shot of a boy playing with pigeons when I was walking through Boston Common.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/pigeon.jpg",
    		"height": 525,
    		"month": 8,
    		"title": "Motion Commotion",
    		"width": 700,
    		"year": 2002
    	}, {
    		"description": "A beautiful day in the Boston Common park.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2994.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "A lazy weekend day.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2999.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Lying Around in Boston Common",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "He could be a hobo, but maybe not.  He's got dirty socks, though.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2992.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Jesus-like guy in Boston common",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "A little girl lined up with a mother duck and her ducklings in Boston Common.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3095.jpg",
    		"height": 600,
    		"month": 3,
    		"title": "Duck, Duck, Duck ...",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "This is the funniest picture ever!  An entire wedding party is being photographed in the Boston Public Garden right in front of this sign!",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3097.JPG",
    		"height": 600,
    		"month": 5,
    		"title": "Please Stay off the Grass!",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "in the Boston Public Garden",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3012.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Flowers and George Washington Monument",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "in the Boston Public Garden",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3014.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Flowers and George Washington Monument (vertical)",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "Some historic buildings in the Beacon Hill neighborhood (really rich!)",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1515.JPG",
    		"height": 800,
    		"month": 9,
    		"title": "Quaint Boston neighborhood",
    		"width": 600,
    		"year": 2005
    	}, {
    		"description": "Typical homes in the Beacon Hill neighborhood (really rich!)",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3078.JPG",
    		"height": 600,
    		"month": 5,
    		"title": "Beacon Hill Real Estate",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "It's so amazing how the streets in the Beacon Hill neighborhood of Boston are so darn quiet, when there is a bustling city just a few blocks nearby.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3082.JPG",
    		"height": 600,
    		"month": 5,
    		"title": "Beacon Hill Street",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Selling trinkets and stuff from his van parked in Chinatown.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3309.JPG",
    		"height": 600,
    		"month": 5,
    		"title": "Chinatown Van Vendor",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Home of fancy restaurants, specialty stores, and rich people.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3898.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Ultra-Posh Newbury Street",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Some dude is taking a nap with his mouth wide open on the steps of the Boston Public Library.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3909.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Mouth Agape at the Boston Public Library",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Kids skateboarding near the historic Trinity Church in Copley Square",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3038.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Skateboarders at Trinity Church",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "The historic Trinity Church and the modern John Hancock building in Copley Square",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2694.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Old and New Juxtaposition",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "Self-explanatory",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2700.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "John Hancock Towering Over Copley Square",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "The names of several famous scientists are engraved in stone at the entrance of the Boston Public Library",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3913.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Science Pimps at Boston Public Library",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "Some menacing-looking statue in Copley Square right next to the Trinity Church",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3932.jpg",
    		"height": 800,
    		"month": 7,
    		"title": "Huge Preacher Duo",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "$$$",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3957.jpg",
    		"height": 800,
    		"month": 7,
    		"title": "Ferrari on Boylston",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "This is crazy!  There's a freaking restaurant in front of the Emporio Armani store on Newbury Street",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3958.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Shopping and Dining at Armani",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "I think these are gnomes.  Just hangin' out on the steps in front of a shop on Newbury Street.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3962.jpg",
    		"height": 800,
    		"month": 7,
    		"title": "Gnomes! Gnomes!",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "Hanging out on the Charles right near the Harvard (Mass. Ave.) bridge.  The Esplanade and Longfellow Bridge can be seen in the distant background.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/img_3963.jpg",
    		"height": 800,
    		"month": 7,
    		"title": "Boat on the Charles River",
    		"width": 600,
    		"year": 2006
    	}, {
    		"description": "A view of Cambridge and part of the MIT campus taken from the Boston side of the Charles River at sunset. <i>(Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_0377.jpg",
    		"height": 600,
    		"month": 12,
    		"title": "Sunset over Cambridge",
    		"width": 800,
    		"year": 2003
    	}, {
    		"description": "A snowy winter sunset over Boston taken from my Cambridge apartment. <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1728.JPG",
    		"height": 600,
    		"month": 12,
    		"title": "Snowy Sunset over Boston",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "This was taken on Memorial Drive right along the Charles River.  I really like the amazing sunrise colors and the entire city in silhouette.  The white streak on the left side was made by the headlights of an incoming car. <i>(Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/sunrise.jpg",
    		"height": 525,
    		"month": 12,
    		"title": "Sunrise over Boston",
    		"width": 700,
    		"year": 2002
    	}, {
    		"description": "The John Hancock building is on the left and the Prudential building is on the right.  Notice the full moon next to the Prudential. <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_0035.jpg",
    		"height": 600,
    		"month": 9,
    		"title": "Full moon sunset",
    		"width": 800,
    		"year": 2003
    	}, {
    		"description": "The John Hancock building has a vertical groove that reflects the sunset in a golden red glow.  Notice the really red area at the base of the building (on the left side). <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_3256.jpg",
    		"height": 600,
    		"month": 4,
    		"title": "Glowing beam of light",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "The beautiful Back Bay region of Boston at night with the Charles River in the middle and Memorial Drive in Cambridge in the foreground. <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_0131.jpg",
    		"height": 600,
    		"month": 6,
    		"title": "Nighttime on the Charles River",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "Selected lights were turned on in the Prudential Building to cheer on the Boston Red Sox baseball team as they competed for the ALCS title.  They ended up losing to the Yankees that year, but won in the following year in an amazing series. <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_0166.jpg",
    		"height": 800,
    		"month": 10,
    		"title": "Cheering for the Red Sox",
    		"width": 600,
    		"year": 2003
    	}, {
    		"description": "This picture was taken in the courtyard of my apartment building after a big snow storm. <i>(Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1713.JPG",
    		"height": 800,
    		"month": 12,
    		"title": "Snowy courtyard",
    		"width": 600,
    		"year": 2005
    	}, {
    		"description": "The Boston Celtics play the Indiana Pacers in the NBA playoffs in the Fleet Center. <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_0957.jpg",
    		"height": 600,
    		"month": 4,
    		"title": "Celtics game at the Fleet Center",
    		"width": 800,
    		"year": 2004
    	}, {
    		"description": "The MacGregor House kids play some volleyball on Thompson Island (a few minutes from Boston by boat) during our annual dorm clambake. <i>(Thompson Island - Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_1974.jpg",
    		"height": 600,
    		"month": 9,
    		"title": "Island Volleyball Action",
    		"width": 800,
    		"year": 2004
    	}, {
    		"description": "A subway performer plays the guitar and sings in the Government Center station <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1268.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Subway Musician",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "An anti-Bush protester plays music and hands out flyers to protest the administration and the war in Iraq. <i>(Harvard Square - Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_0105.jpg",
    		"height": 800,
    		"month": 6,
    		"title": "Anti-Bush protester",
    		"width": 600,
    		"year": 2005
    	}, {
    		"description": "A vocal Christian warns the world of the consequences of not being Christian.  A bald man with an impressive mustache chats with that guy. <i>(Harvard Square - Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1305.jpg",
    		"height": 800,
    		"month": 7,
    		"title": "Hardcore Christian in Harvard Square",
    		"width": 600,
    		"year": 2005
    	}, {
    		"description": "A street performer gets audience members to tie him up and then escapes from the ropes <i>(Harvard Square - Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1309.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Escape Artist in Harvard Square",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "A really old cemetary with tombstones dating from the 1600's. <i>(Harvard Square - Cambridge, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/s70_IMG_1323.jpg",
    		"height": 600,
    		"month": 7,
    		"title": "Really old cemetary",
    		"width": 800,
    		"year": 2005
    	}, {
    		"description": "We went near Boston's Logan airport one day because my friends wanted to photograph airplanes landing.  While they were doing that, I photographed them photographing airplanes landing.  Yep, I'm weird. <i>(Boston, MA)</i>",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/plane.jpg",
    		"height": 600,
    		"month": 8,
    		"title": "Man and Plane",
    		"width": 450,
    		"year": 2002
    	}, {
    		"description": "in downtown Boston.  Live music, beer (Guiness!), and middle-aged men.  Mmmm",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2874.JPG",
    		"height": 600,
    		"month": 4,
    		"title": "Music at the Black Rose Pub",
    		"width": 800,
    		"year": 2006
    	}, {
    		"description": "The beers that Kevin and I downed are chillin' on the table having a good time listening to music.",
    		"filename": "http://www.pgbovine.net/new-galleries/boston/thumbnails/IMG_2882.JPG",
    		"height": 800,
    		"month": 4,
    		"title": "Beers Enjoying Live Pub Music",
    		"width": 600,
    		"year": 2006
    	}]
    };
    vm.title = vm.datas.folderName;
    vm.galleryname = vm.datas.galleryName;
    vm.images = vm.datas.images;
    vm.bigger = false;

    vm.getPosition = getPosition;

    // functions

    function getPosition(image) {
      var positions = image.getBoundingClientRect();
      console.log(positions);
    }
  }
}
